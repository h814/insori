\chapter{Ethische Herausforderungen im Wissensmanagement}
\label{chap:ethik}
Dieses Kapitel befasst sich mit den ethischen Einflüssen und Herausforderungen, die durch die Anwendung von Wissensmanagement in Unternehmen entstehen.

\section{Ethik}
Die Ethik ergibt sich nach \citeauthor{Akhavan.2013} aus der menschlichen Natur. Sie steht für Normen und Ideale in einer Gruppe oder Gemeinschaft. \parencite{Akhavan.2013}
Deshalb ist sie selten Gegenstand einer absoluten Norm und kann variieren \parencite{Rechberg.2013}.
Ethik wurde entwickelt, um das moralische Leben des Einzelnen, seine Entscheidungen und sein Verhalten in verschiedenen Situationen zu untersuchen \parencite{Tseng.2011}.
Aus der Ethik entstand die Wirtschaftsethik. Dieser Zweig der Ethik ist dafür zuständig, die moralische Qualität der Entscheidungsfindung und Leistung auf allen Unternehmensebenen zu verbessern \parencite{Akhavan.2013}. \\

\noindent Es gibt verschiedene Betrachtungsmodelle in der Ethik, zum Beispiel die Teleologie und die Deontologie.
Teleologie beschreibt den Ansatz, welcher das Ende oder die Folgen einer Handlung nach gut oder schlecht bewertet.
Der deontologische Ansatz hingegen betrachtet die Handlung an sich als Grundlage der Ethik \parencite{Rechberg.2013}.

\section{Ethische Probleme im Wissensmanagement}
Nach \citeauthor{Lutge.2002} darf Ethik im Wissensmanagement nicht als Bremse dienen. Stattdessen sollte sie dazu beitragen, Wissensmanagement und seine Prozesse zu verbessern.
Ethik soll dabei helfen den Herausforderungen moralischer Natur entgegenzuwirken. Die Prozesse im Wissensmanagement müssen dafür transparent gestaltet und die Akzeptanz hoch gehalten werden \parencite{Lutge.2002}. \\

\noindent Nach \citeauthor{Lutge.2002}, welcher sich auf \citeauthor{Hubig.1998} beruft, gibt es aus ethischer Sicht vier Problemstellungen, die das Wissensmanagement betreffen.
Zum einen ist es bei komplexem Wissen immer schwieriger das Wissen auf Richtigkeit zu überprüfen. Auch die historische Entwicklung und Zusammensetzung von Wissen ist immer schwerer nachvollziehbar.
Als zweite Schwierigkeit erwähnt \citeauthor{Lutge.2002}, dass es immer mehr Expertenmeinungen gibt, die sich widersprechen. Das regt einen gewissen Zweifel an der Korrektheit des Wissens und der Information an.
Den dritten Punkt, den \citeauthor{Hubig.1998} als potentielles Problem sieht, ist die Erstellung von Nutzerprofilen von Unternehmen. Diese sammeln im Rahmen des Wissensmanagements gewisse Informationen über die Nutzer.
Davon betroffen sind insbesondere Wissensmanagementsysteme mit informationstechnologischer Unterstützung.
Das Problem dabei ist, dass durch diese Profilerstellungen Stereotyp belastete Profile entstehen.
Als letzter Punkt wird aufgeführt, dass durch die verbreitete Nutzung von IT-Systemen die Kommunikations- und Interaktionskompetenz sinkt.
Dadurch sieht man sich selbst im Mittelpunkt einer Interaktion \parencite{Lutge.2002, Hubig.1998}.
Das Nachlassen von Kommunikations- und zwischenmenschlichen Fähigkeiten hat wiederum einen negativen Einfluss auf die Gesellschaft und das gesellschaftliche Zusammenleben.
Das Wiederum führt im Umkehrschluss zu einem schlechteren Wissensmanagementprozess, da dieser die vorangegangenen Faktoren benötigt (\autoref{chap:wissensmanagement:sec:unternehmen}). \\

\noindent Die vorangegangen Punkte sind recht oberflächlich gehalten und bieten keinen direkten Lösungsweg oder Verbesserungsvorschlag. Die Autoren \citeauthor{Evans.2011} sowie \citeauthor{Rechberg.2013}, gehen etwas detaillierter auf die ethischen Problemstellungen ein und zeigen auch Lösungswege für diese Probleme auf.
Die wichtigste Frage für \citeauthor{Evans.2011} ist, wem das Wissen gehört. Beide Autorenpaare sind sich dabei einig, dass das Wissen den Angestellten selbst gehört.
Allerdings sehen \citeauthor{Evans.2011} die Grenze zwischen, das Wissen gehört der Person oder es gehört dem Unternehmen etwas kritischer, wenn das Wissen am Arbeitsplatz entsteht.
\citeauthor{Rechberg.2013} sind der Meinung, dass ohne die Angestellten, die das Wissen besitzen, teilen oder sich aneignen, ein Unternehmen nicht richtig funktionieren, oder gar Wissensmanagement betreiben kann.
Die Autoren sehen es kritisch, dass es keine genauen rechtlichen Regelungen gibt, welche das Wissen von Personen schützen.
Nach \citeauthor{Rechberg.2013} wäre beispielsweise der Arbeitsvertrag eine Möglichkeit eine gewisse Sicherheit und Vertrauensbasis zu schaffen, denn aktuell spiegelt dieser nur eine sehr eingeschränkte und schlecht definierte Definition, bis gar keine, für Eigentumsrechte an Wissen wieder \parencite{Rechberg.2013}.
Das andere Problem mit dem Besitz des Wissens ist, dass Unternehmen Wissen oft als Vermögenswert ansehen. Das führt oft dazu, dass Unternehmen versuchen ihre Mitarbeiter zu übervorteilen um Profit zu generieren oder die Bedürfnisse des Unternehmens denen der Mitarbeiter vorziehen.
Dadurch wird das Vertrauen zwischen Unternehmen und Mitarbeiter gestört, was wiederum dazu führt, dass Mitarbeiter ihr Wissen weniger gerne teilen wollen \parencite{Rechberg.2013}.
Eine Übervorteilung kann zum Beispiel sein, dass Unternehmen das Wissen ihrer Mitarbeiter ausnutzen, ohne sie dafür zu belohnen (abgesehen von der Vergütung) \parencite{Rechberg.2013, Evans.2011}. \\

\noindent Ein anderes ethisches Problem ist, dass das Wissen, wenn es geteilt wurde, dem Unternehmen frei zur Verfügung steht und dort genutzt werden kann.
Dadurch kann es vom Unternehmen manipuliert, verzerrt oder zurück gehalten werden \parencite{Evans.2011}.
Das findet zum Beispiel Anwendung, wenn politisch agiert oder gewisse Lobbies unterstützt werden sollen.
Des Weiteren wird durch das im Unternehmen nun frei verfügbare Wissen für die Konkurrenz die Möglichkeit geboten, Wissen zu stehlen (Industriespionage), oder es wird anderen ermöglicht das Wissen unangemessen offen zu legen \parencite{Evans.2011}.
Eine unangemessene Offenlegung könnte zum Beispiel Whistleblowing sein \parencite{Evans.2011}. \\

\noindent Ein Punkt in welchem sich \citeauthor{Evans.2011} sowie \citeauthor{Lutge.2002} einig sind, ist der Datenmissbrauch, welcher mit dem Wissensmanagement einhergehen kann.
Gerade heute in einem Zeitalter in welchem viele Daten erhoben werden, kann es schnell passieren, dass die Privatsphäre der Mitarbeiter missachtet wird und wie von \citeauthor{Lutge.2002} angesprochen, Mitarbeiterprofile erstellt werden \parencite{Evans.2011, Lutge.2002}. \\

\noindent Das aber wohl größte Problem ist, dass \emph{\uline{Wissen in Unternehmen als Macht angesehen wird und Wissen oft die Quelle für Macht ist.}} Das kann sich negativ auswirken, wenn Übergeordnete Druck auf Untergebene ausüben.
Dadurch verschlechtert sich das Arbeitsklima und das Vertrauen, was wiederum zum eingeschränkten Teilen von Wissen führt. Nach \citeauthor{Rechberg.2013} teilt sich diese Macht in persönliche und soziale Macht auf.
Persönliche Macht stellt hierbei die Macht dar, nicht von anderen kontrolliert werden zu können und zu tun und zu bekommen was man will. Soziale Macht hingegen ist die Fähigkeit andere zu beeinflussen und Kontrolle über sie auszuüben.
Der Besitz von Wissen soll dem Besitzer persönliche Macht schaffen und ihm somit Kontrolle über seine Karriere und die Organisation geben. Wenn er es teilen würde, wäre diese Macht verloren.
Das kann im Unternehmen zu Spannungen führen \parencite{Rechberg.2013}.
Wenn ein Mitarbeiter sich zum Teilen des Wissens entscheidet und seine Macht an das Unternehmen weitergibt, stellt er das Unternehmen in eine Machtposition ihm gegenüber \parencite{Rechberg.2013, Evans.2011}.
Diese Machtposition kann zum Entlassen des Mitarbeiters führen, da er nun nicht weiter benötigt wird. Sein Wissen steht dem Unternehmen jetzt zur Verfügung. Die Angst dass dieser Fall eintreten könnte, ist ein weiterer Grund für Mitarbeiter, an ihrer persönlichen Macht festzuhalten.
Aber auch andersherum ist es möglich. Es kann passieren, dass Machthaber Angst um ihre Position haben und deshalb das Wissen von Untergebenen horten oder manipulieren \parencite{Rechberg.2013}. \\

\noindent \citeauthor{Evans.2011} haben einen anderen Lösungsansatz als \citeauthor{Rechberg.2013}. Sie sind der Meinung man sollte die Ansätze inklusiv-exklusiv und transparent-opak verfolgen.
Der Ansatz inklusiv-exklusiv soll die Frage beantworten, ob ein System für das öffentliche oder kollektive Wohl erstellt worden ist. Der transparent-opak Ansatz soll den Mitarbeitern mehr Transparenz verschaffen.
Die Mitarbeiter sollen wissen, dass Wissen über sie und von ihnen gesammelt wird, wann und wie dieser Prozess stattfindet, wie diese Daten verwendet werden und was die Folgen dessen sein können.
Sie sagen, man sollte vom Wissen zur Weisheit übergehen, da Weisheit Wissen mit einer ethischen Perspektive ist \parencite{Evans.2011}. \\
\noindent \citeauthor{Rechberg.2013} haben einen etwas konkreteren Lösungsvorschlag. Sie schlagen einen moralischen Vertrag zwischen Mitarbeiter und Unternehmen vor. Dieser muss einen ethischen Ansatz verfolgen und sollte die Rechte und Pflichten von Arbeitgebern und -nehmern regeln.
Alles in allem soll ein fairer ethischer Rahmen geschaffen werden, welcher ein ethisches Organisationsklima schafft.
Dazu sollten vom Unternehmen Belohnungen für Wissensfortschritt eingeführt werden und Anerkennung und Status an die Person/en zurückgegeben werden, welche den Fortschritt erreicht hat/haben.
Bei starken oder sehr profitablen Fortschritten kann auch über eine Beförderung oder Gehaltserhöhung gesprochen werden. Durch diese Schritte soll das Vertrauen, die Fairness und Gerechtigkeit steigen.
Als Resultat soll weniger Wissen gehortet werden und ein innovatives Verhalten gefördert werden \parencite{Rechberg.2013}. \\

\noindent \citeauthor{Rechberg.2013}, als auch \citeauthor{Evans.2011} sind der Meinung, dass oftmals nur die positiven Seiten und Einflüsse des Wissensmanagements dargestellt werden.
Allerdings gibt es auch einige negative Seiten und ethische Aspekte, welche in einem Unternehmen für ein erfolgreiches Wissensmanagement auf jeden Fall beachtet werden müssen. Letzten Endes ist das Teilen von Wissen eine freiwillige Sache auf die das Unternehmen für seinen Erfolg angewiesen ist \parencite{Rechberg.2013, Evans.2011}.